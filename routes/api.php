<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function () {
    /*
     * Categories
     */
    Route::get('categories', "CategoryController@list");
    Route::get('categories/tree', "CategoryController@tree");
    Route::get('categories/{id}', "CategoryController@show");
    Route::post('categories', "CategoryController@store");
    Route::post('categories/{id}/delete', "CategoryController@delete");
    /*
     * Uploads
     */
    Route::post('upload/images', "UploadController@images");
    /*
     * Products
     */
    Route::get('products', "ProductController@list");
    Route::get('products/{id}', "ProductController@show");
    Route::post('products', "ProductController@store");
    Route::post('products/{id}/delete', "ProductController@delete");
    /*
     * Specifications
     */
    Route::get('specifications', "SpecificationController@list");
    Route::get('specifications/{id}', "SpecificationController@show");
    Route::post('specifications', "SpecificationController@store");
    Route::post('specifications/{id}/delete', "SpecificationController@delete");
    /*
     * Orders
     */
    Route::get('orders', "OrderController@list");
    Route::get('orders/{id}', "OrderController@show");
    Route::post('orders', "OrderController@store");
    Route::post('orders/{id}/delete', "OrderController@delete");
});

/*
 * Login
 */
Route::post('login', "LoginController@login");

