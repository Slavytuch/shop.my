<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Category;

class CategoryController extends EntityController
{
    protected $entity = "\App\Models\Category";

    public function tree()
    {
        return Category::top()->with('children')->get();
    }
}
