<?php

namespace App\Http\Controllers;

use App\Models\OrderProduct;
use Illuminate\Http\Request;

class OrderController extends EntityController
{
    protected $entity = "\App\Models\Order";
    protected $relations = ["products"];

    public function saveRelation(Request $request, $entity)
    {
        $entity->products()->delete();
        $entity->products()->saveMany(collect($request->products)->map(function ($item) use ($entity) {
            return new OrderProduct([
                'name' => $item['name'],
                'image' => $item['image'],
                'price' => $item['price'],
                'sale_price' => $item['sale_price'],
                'option_name' => $item['option_name'],
                'option_value' => $item['option_value'],
                'sort_order' => $item['sort_order'],
                'order_id' => $entity->id,
            ]);
        }));
    }
}
