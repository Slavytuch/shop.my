<?php

namespace App\Http\Controllers;

use App\Models\ProductImage;
use Illuminate\Http\Request;


class ProductController extends EntityController
{
    protected $entity = "\App\Models\Product";
    protected $relations = ["specifications", "categories", "images"];

    protected function saveRelation(Request $request, $entity)
    {
        $entity->specifications()->sync(collect($request->specifications)->mapWithKeys(function ($specification) {
            return [$specification["pivot"]['specification_id'] => ["value" => $specification["pivot"]["value"], "sort_order" => $specification["pivot"]["sort_order"]]];
        }));

        $entity->categories()->sync(collect($request->categories)->map(function ($category) {
            return isset($category["id"]) ? $category["id"] : $category;
        }));

        $entity->images()->delete();
        $entity->images()->saveMany(collect($request->images)->map(function ($item) use ($entity) {
            return new ProductImage(['product_id' => $entity->id, 'image' => $item['image']]);
        }));
    }
}
