<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function images(Request $request)
    {
        $path = $request->file("image")->store("public");
        return ["success" => true, "file" => Storage::url($path)];
    }
}
