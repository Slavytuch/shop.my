<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EntityController extends Controller
{
    protected $entity;
    protected $relations = [];

    public function list()
    {
        return $this->entity::with($this->relations)->get();
    }

    public function show($id)
    {
        return $this->entity::with($this->relations)->find($id);
    }

    public function store(Request $request)
    {
        $entity = $this->entity::updateOrCreate(['id' => $request->id], $request->all());
        $this->saveRelation($request, $entity);
        return ['success' => true, 'entity' => $entity];
    }

    public function delete($id)
    {
        $this->entity::find($id)->delete();
        return ["success" => true];
    }

    protected function saveRelation(Request $request, $entity)
    {

    }
}