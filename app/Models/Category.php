<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ["name", "parent_id", "description", "image", "url", "sort_order", "status"];

    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id')->with('children');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id');
    }

    public function scopeTop($query)
    {
        return $query->where('parent_id', 0);
    }
}
