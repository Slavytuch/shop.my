<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ["name", "description", "image", "price", "sale_price", "sort_order", "status"];

    public function specifications()
    {
        return $this->belongsToMany('\App\Models\Specification')->withPivot(["value", "sort_order"])->orderBy("sort_order");
    }

    public function categories()
    {
        return $this->belongsToMany('\App\Models\Category');
    }

    public function images()
    {
        return $this->hasMany('\App\Models\ProductImage');
    }
}
