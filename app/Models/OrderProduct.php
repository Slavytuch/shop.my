<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    protected $fillable = ["name", "image", "price", "sale_price", "option_name", "option_value", "order_id", "sort_order"];
}
