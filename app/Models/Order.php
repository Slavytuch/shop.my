<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ["customer_name", "customer_phone_number", "customer_email"];
    protected $appends = ["total"];

    public function products()
    {
        return $this->hasMany("\App\Models\OrderProduct");
    }

    public function getTotalAttribute()
    {
        return $this->products->reduce(function ($total, $product) {
            return $total + ($product['sale_price'] ? $product['sale_price'] : $product['price']);
        });
    }
}
