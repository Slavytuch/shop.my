<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            [
                "customer_name" => "Имя заказчика 1",
                "customer_phone_number" => "Номер телефона заказчика 1",
                "customer_email" => "Почта заказчика 1",
            ],
            [
                "customer_name" => "Имя заказчика 2",
                "customer_phone_number" => "Номер телефона заказчика 2",
                "customer_email" => "Почта заказчика 2",
            ],
            [
                "customer_name" => "Имя заказчика 3",
                "customer_phone_number" => "Номер телефона заказчика 3",
                "customer_email" => "Почта заказчика 3",
            ],
        ]);
    }
}
