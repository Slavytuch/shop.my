<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                "name" => "Продукт 1",
                "description" => "Описание продукта 1",
            ],
            [
                "name" => "Продукт 2",
                "description" => "Описание продукта 2",
            ],
            [
                "name" => "Продукт 3",
                "description" => "Описание продукта 3",
            ]
        ]);
    }
}
