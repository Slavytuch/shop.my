<?php

use Illuminate\Database\Seeder;

class SpecificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('specifications')->insert([
            [
                "name" => "Спецификация 1",
            ],
            [
                "name" => "Спецификация 2",
            ],
            [
                "name" => "Спецификация 3",
            ]
        ]);
    }
}
