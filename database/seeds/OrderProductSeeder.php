<?php

use Illuminate\Database\Seeder;

class OrderProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_products')->insert([
            [
                "name" => "Название продукта 1",
                "price" => 10,
                "sale_price" => 5,
                "option_name" => "Название опции 1",
                "option_value" => "Значение опции 1",
                "order_id" => 1,
                "sort_order" => 0
            ],
            [
                "name" => "Название продукта 2",
                "price" => 10,
                "sale_price" => 5,
                "option_name" => "Название опции 2",
                "option_value" => "Значение опции 2",
                "order_id" => 1,
                "sort_order" => 0
            ]
        ]);
    }
}
