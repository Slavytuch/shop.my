<?php

use Illuminate\Database\Seeder;

class CategoryProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_product')->insert([
            [
                "category_id" => 1,
                "product_id" => 1
            ],
            [
                "category_id" => 1,
                "product_id" => 2
            ],
            [
                "category_id" => 2,
                "product_id" => 1
            ]
        ]);
    }
}
