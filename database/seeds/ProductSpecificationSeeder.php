<?php

use Illuminate\Database\Seeder;

class ProductSpecificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_specification')->insert([
            [
                "product_id" => 1,
                "specification_id" => 1,
                "value" => "Спецификация 1 продукта 1",
            ],
            [
                "product_id" => 1,
                "specification_id" => 2,
                "value" => "Спецификация 2 продукта 1",
            ],
            [
                "product_id" => 2,
                "specification_id" => 1,
                "value" => "Спецификация 1 продукта 2",
            ],
        ]);
    }
}
