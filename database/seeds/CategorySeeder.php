<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                "name" => "Категория 1",
                "description" => "Описание 1"
            ],
            [
                "name" => "Категория 2",
                "description" => "Описание 2"
            ],
            [
                "name" => "Категория 3",
                "description" => "Описание 3"
            ]
        ]);

        DB::table('categories')->insert([
            [
                "name" => "Категория 1.1",
                "description" => "Описание 1.1",
                "parent_id" => 1
            ],
            [
                "name" => "Категория 1.2",
                "description" => "Описание 1.2",
                "parent_id" => 1
            ],
            [
                "name" => "Категория 1.3",
                "description" => "Описание 1.3",
                "parent_id" => 1
            ]
        ]);

        DB::table('categories')->insert([
            [
                "name" => "Категория 1.1.1",
                "description" => "Описание 1.1.1",
                "parent_id" => 4
            ],
            [
                "name" => "Категория 1.1.2",
                "description" => "Описание 1.1.2",
                "parent_id" => 4
            ],
        ]);
    }
}
